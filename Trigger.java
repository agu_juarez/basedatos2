import java.util.*;

public class Trigger {
    
    private String name;
    private String momentTrigger;

    public Trigger() {
        this.name = null;
        this.momentTrigger = null;
    }

    public Trigger(String nameTrigger, String momentTrigger) {
        this.name = nameTrigger;
        this.momentTrigger = momentTrigger;
    }

    public String getNameTrigger() {
        return this.name;
    }

    public void setNameTrigger(String newNameTrigger) {
        this.name = newNameTrigger;
    }

    public String getMomentTrigger() {
        return this.momentTrigger;
    }

    public void setMomentTrigger(String newMomentTrigger) {
        this.momentTrigger = newMomentTrigger;
    }
}