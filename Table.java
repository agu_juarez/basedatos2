import java.util.*;

public class Table {

    private String name;
    private LinkedList<Column> setColumn;
    private LinkedList<String> setPrimaryKeys;
    private LinkedList<String> setUniqueKeys;
    private LinkedList<Trigger> setTrigger;

    public Table() {
        this.name = null;
        this.setColumn = null;
        this.setPrimaryKeys = null;
        this.setUniqueKeys = null;
        this.setTrigger = null;
    }

    public Table(String nameTable, LinkedList<Column> setColumn, LinkedList<Trigger> setTrigger, LinkedList<String> setPrimaryKeys, LinkedList<String> setUniqueKeys) {
        this.name = nameTable;
        this.setColumn = setColumn;
        this.setPrimaryKeys = setPrimaryKeys;
        this.setUniqueKeys = setUniqueKeys;
        this.setTrigger = setTrigger;
    }

    public String getNameTable() {
        return this.name;
    }

    public void setNameTable(String newNameTable) {
        this.name = newNameTable;
    }

    public LinkedList<Column> getSetColumn() {
        return this.setColumn;
    }

    public void setSetColumn(LinkedList<Column> setColumn) {
        this.setColumn = setColumn;
    }

    public LinkedList<String> getSetPrimaryKeys() {
        return this.setPrimaryKeys;
    }

    public void setSetPrimaryKeys(LinkedList<String> setPrimaryKeys) {
        this.setPrimaryKeys = setPrimaryKeys;
    }

    public LinkedList<String> getSetUniqueKeys() {
        return this.setUniqueKeys;
    }

    public void setSetUniqueKeys(LinkedList<String> setUniqueKeys) {
        this.setUniqueKeys = setUniqueKeys;
    }

    public LinkedList<Trigger> getSetTrigger() {
        return this.setTrigger;
    }

    public void setSetTrigger(LinkedList<Trigger> setTrigger) {
        this.setTrigger = setTrigger;
    }
}