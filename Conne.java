import java.sql.*;
import java.util.*;

import javax.print.attribute.SetOfIntegerSyntax;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

public class Conne {

    private String host;
    private String nameDB;
    private String user;
    private String password;

    public String getNameDB() {
        return this.nameDB;
    }

    public void requestData() {
        Scanner sc = new Scanner(System.in);
        System.out.println("INGRESE EL HOST DE SU DB - Por ejemplo: localhost:3306");
        this.host = sc.nextLine();
        System.out.println("INGRESE EL NOMBRE DE LA BD A CONECTARSE:");
        this.nameDB = sc.nextLine();
        System.out.println("INGRESE EL USUARIO");
        this.user = sc.nextLine();
        System.out.println("INGRESE LA CONTRASEÑA:");
        this.password = sc.nextLine();
    }

    public Connection establishConnection(Connection connection) {
        try {
            String url = "jdbc:mysql://"+this.host+"/"+this.nameDB;
     		String username = this.user;
            String password = this.password;
            connection = DriverManager.getConnection(url, username, password);

            return connection;
        } catch (SQLException e) {
            System.out.println("-------- FALLA EN LA CONEXION --------");
			e.printStackTrace();
			return null;
        }
    }

    private LinkedList<Column> generateSetColumnsForTable(Connection connection, String nameTable) {
        LinkedList<Column> setColumns = new LinkedList<Column>();

        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSetColumns = metaData.getColumns(this.nameDB, null, nameTable, null);

            while(resultSetColumns.next()) {
                Column column = new Column();
                String nameColumn = resultSetColumns.getString(4);
                String typeColumn = resultSetColumns.getString(6);
                column.setNameColumn(nameColumn);
                column.setTypeColumn(typeColumn);
                setColumns.add(column);
            }

            return setColumns;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private LinkedList<String> generateSetPrimaryKeysForTable(Connection connection, String nameTable) {
        LinkedList<String> setPrimaryKeys = new LinkedList<String>();

        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSetPrimaryKeys = metaData.getPrimaryKeys(this.nameDB, null, nameTable);

            while(resultSetPrimaryKeys.next()) {
                String namePrimaryKey = resultSetPrimaryKeys.getString(4);
                setPrimaryKeys.add(namePrimaryKey);
            }

            return setPrimaryKeys;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private LinkedList<String> generateSetUniqueKeysForTable(Connection connection, String nameTable) {
        LinkedList<String> setUniqueKeys = new LinkedList<String>();

        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSetUniqueKeys = metaData.getIndexInfo(this.nameDB, null, nameTable, true, false);

            while(resultSetUniqueKeys.next()) {
                if(resultSetUniqueKeys.getBoolean(4) == false) {
                    String nameUniqueKey = resultSetUniqueKeys.getString(9);
                    setUniqueKeys.add(nameUniqueKey);
                }
            }

            return setUniqueKeys;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private LinkedList<Trigger> generateSetTriggerForTable(Connection connection, String nameTable) {
        LinkedList<Trigger> setTrigger = new LinkedList<Trigger>();

        try {
            Statement statement = connection.createStatement();
            String query = "SELECT TRIGGER_NAME, ACTION_TIMING FROM information_schema.TRIGGERS WHERE EVENT_OBJECT_TABLE = "+"'"+nameTable+"'";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                Trigger trigger = new Trigger();
                String nameTrigger = resultSet.getString("TRIGGER_NAME");
                String momentTrigger = resultSet.getString("ACTION_TIMING");
                trigger.setNameTrigger(nameTrigger);
                trigger.setMomentTrigger(momentTrigger);
                setTrigger.add(trigger);
            }

            return setTrigger;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public LinkedList<Table> generateSetTables(Connection connection, String name) {
        LinkedList<Table> setTables = new LinkedList<Table>();

        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSetTables = metaData.getTables(name, null, "%", null);

            while(resultSetTables.next()) {
                Table table = new Table();
                String nameTable = resultSetTables.getString(3);
                table.setNameTable(nameTable);

                table.setSetColumn(generateSetColumnsForTable(connection, nameTable));
                table.setSetPrimaryKeys(generateSetPrimaryKeysForTable(connection, nameTable));
                table.setSetUniqueKeys(generateSetUniqueKeysForTable(connection, nameTable));
                table.setSetTrigger(generateSetTriggerForTable(connection, nameTable));
                setTables.add(table);
            }

            return setTables;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private LinkedList<String> generateSetParameter(Connection connection, String nameProcedure) {
        LinkedList<String> setParameter = new LinkedList<String>();

        try {
            Statement statement = connection.createStatement();
            String query = "SELECT DATA_TYPE FROM information_schema.PARAMETERS WHERE SPECIFIC_NAME = "+"'"+nameProcedure+"' AND ORDINAL_POSITION != 0";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                String typeParameter = resultSet.getString("DATA_TYPE");
                setParameter.add(typeParameter);
            }

            return setParameter;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getTypeReturnProcedure(Connection connection, String nameProcedure) {
        String typeReturn = null;

        try {
            Statement statement = connection.createStatement();
            String query = "SELECT DATA_TYPE FROM information_schema.PARAMETERS WHERE SPECIFIC_NAME = "+"'"+nameProcedure+"' AND ORDINAL_POSITION = 0";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                typeReturn = resultSet.getString("DATA_TYPE");
            }

            return typeReturn;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public LinkedList<Procedure> generateSetProcedureAndFunction(Connection connection, String name) {
        LinkedList<Procedure> setProcedure = new LinkedList<Procedure>();

        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSetProdecure  = metaData.getProcedures(name, null, "%");

            while(resultSetProdecure.next()) {
                Procedure procedure = new Procedure();

                String nameProcedure = resultSetProdecure.getString(9);
                procedure.setNameProdecure(nameProcedure);

                procedure.setSetParameter(generateSetParameter(connection, nameProcedure));
                procedure.setTypeReturn(getTypeReturnProcedure(connection, nameProcedure));

                setProcedure.add(procedure);
            }

        return setProcedure;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}