import java.util.*;

public class Procedure {

    private String name;
    private LinkedList<String> setParameter;
    private String typeReturn;

    public Procedure() {
        this.name = null;
        this.setParameter = null;
        this.typeReturn = null;
    }

    public String getNameProcedure() {
        return this.name;
    }

    public void setNameProdecure(String name) {
        this.name = name;
    }

    public LinkedList<String> getSetParameter() {
        return this.setParameter;
    }

    public void setSetParameter(LinkedList<String> setParameter) {
        this.setParameter = setParameter;
    }

    public String getTypeReturn() {
        return this.typeReturn;
    }

    public void setTypeReturn(String typeReturn) {
        this.typeReturn = typeReturn;        
    }
}