import java.util.*;

public class Column {
    
    private String name;
    private String type;

    public Column() {
        this.name = null;
        this.type = null;
    }

    public Column(String nameColumn, String typeColumn) {
        this.name = nameColumn;
        this.type = typeColumn;
    }

    public String getNameColumn() {
        return this.name;
    }

    public void setNameColumn(String newNameColumn) {
        this.name = newNameColumn;
    }

    public String getTypeColumn() {
        return this.type;
    }

    public void setTypeColumn(String newTypeColumn) {
        this.type = newTypeColumn;
    }
}